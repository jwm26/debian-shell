FROM debian:stable

LABEL maintainer='Nate Childers <nate.childers@duke.edu>'

LABEL vendor='Duke University, Openshift Users Group' \
      architecture='x86_64' \
      summary='debian base image, updated' \
      description='Base image for Debian stable' \
      distribution-scope='private' \
      authoritative-source-url='https://gitlab.oit.duke.edu/linux-at-duke'

LABEL version='1.0' \
      release='1'

RUN apt-get update && \
    apt-get install -y \
      curl \
      httpie \
      rsync

RUN mkdir /.httpie && chgrp -R 0 /.httpie && chmod -R g+rwX /.httpie

CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
